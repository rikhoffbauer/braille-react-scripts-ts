'use strict';

const express = require('express');
const fallback = require('express-history-api-fallback');
const fs = require('fs');
const proxy = require('express-http-proxy');
const bodyParser = require('body-parser');
const paths = require('../config/paths');

const startServer = () => {
  const { PORT = 5000, BACKEND_HOSTNAME, BACKEND_PORT } = process.env;

  const app = express();

  // Add middleware
  app.use(bodyParser.urlencoded({ extended: false }), bodyParser.json());

  // Serve static files
  app.use(express.static(paths.appBuild));

  // Add proxy
  if (BACKEND_HOSTNAME && BACKEND_PORT) {
    const url = `http://${BACKEND_HOSTNAME}:${BACKEND_PORT}`;
    app.use('/api', proxy(url));

    console.log(`Proxying requests to /api to ${url}`);
  }

  app.use(fallback('index.html', { root: paths.appBuild }));

  console.log(`Serving static content from ${paths.appBuild}`);

  // Start listening
  app.listen(PORT);

  console.log(`Listening on port ${PORT}`);
};

if (fs.existsSync(paths.appServer)) {
  // run custom server.js file
  // TODO add support for server.ts file (typescript)
  require(paths.appServer);
} else {
  startServer();
}
