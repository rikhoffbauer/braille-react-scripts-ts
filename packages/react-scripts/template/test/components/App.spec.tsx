import { mount } from "enzyme";
import * as React from "react";
import { Provider } from "react-redux";
import { StaticRouter } from "react-router-dom";
import { createMockStore } from "redux-test-utils";
import App from "../../src/components/App/App";
import Counter from "../../src/components/Counter/Counter";

it("renders without crashing", () => {
    const store = createMockStore({
        counter: {
            count: 0,
        },
    });

    const el = mount(
        <StaticRouter location="/counter" context={{ url: "/counter" }}>
            <Provider store={store}>
                <App/>
            </Provider>
        </StaticRouter>,
    );

    el.find(Counter)
        .find('[data-test-id="increment"]')
        .simulate("click");

    expect(store).toHaveDispatched({ type: "counter/INCREMENT" });
});
