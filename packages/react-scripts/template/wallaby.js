module.exports = function(w) {
    const tsconfig = require('./tsconfig.test');
    const path = require('path');
    process.env.NODE_PATH +=
        path.delimiter +
        path.join(__dirname, 'node_modules') +
        path.delimiter +
        path.join(
            __dirname,
            'node_modules/@braille/react-scripts-ts/node_modules'
        );
    require('module').Module._initPaths();

    return {
        maxConsoleMessagesPerTest: 10000,

        files: [
            'node_modules/@braille/react-scripts-ts/**/*.js',
            'node_modules/@braille/react-scripts-ts/**/*.json',
            'src/**/*',
            'test/**/*',
            'generated/**/*',
            'scripts/**/*',
            '!test/**/*.spec.ts',
            '!test/**/*.spec.tsx',
            'jest.config.json',
            'package.json',
            'tsconfig.json',
            'tsconfig.test.json',
        ],

        tests: ['test/**/*.spec.ts', 'test/**/*.spec.tsx'],

        env: {
            type: 'node',
            runner: 'node',
        },

        testFramework: 'jest',

        setup: function(wallaby) {
            const jestConfig = require('@braille/react-scripts-ts/scripts/utils/createJestConfig')(
                p => require.resolve('@braille/react-scripts-ts/' + p)
            );
            Object.keys(jestConfig.transform || {}).forEach(
                k =>
                    ~k.indexOf('^.+\\.(js|jsx') &&
                    void delete jestConfig.transform[k]
            );
            Object.keys(jestConfig.transform || {}).forEach(
                k =>
                    ~k.indexOf('^.+\\.(ts|tsx') &&
                    void delete jestConfig.transform[k]
            );
            delete jestConfig.testEnvironment;

            jestConfig.globals = { __DEV__: true, NODE_ENV: 'test' };

            wallaby.testFramework.configure(jestConfig);
        },

        compilers: {
            '**/*.ts': w.compilers.typeScript(tsconfig.compilerOptions),
            '**/*.tsx': w.compilers.typeScript(tsconfig.compilerOptions),
            '**/*.js?(x)': w.compilers.babel({
                babel: require('babel-core'),
                presets: ['react-app'],
            }),
        },
    };
};
