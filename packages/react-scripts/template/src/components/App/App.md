App example:

```jsx harmony
const { Provider } = require("react-redux")
const configureStore = require("../../redux/configureStore").default;

const store = configureStore();

<Provider store={store}>
    <App />
</Provider>
```
