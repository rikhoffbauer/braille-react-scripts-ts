import * as React from "react";
import { hot } from 'react-hot-loader'
import { Link, Route, RouteProps, Switch } from "react-router-dom";
import CounterPage from "../../pages/CounterPage/CounterPage";
import HomePage from "../../pages/HomePage/HomePage";
import "./App.css";
import logo from "./logo.svg";

class App extends React.Component {
    public render() {
        return (
            <div className="App">
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo" />
                    <h1 className="App-title">Welcome to React</h1>
                </header>
                <div>
                    <Link to={"/"}>Home</Link>
                    <Link to={"/counter"}>Counter</Link>
                </div>
                <Switch>
                    <Route<RouteProps>
                        exact={true}
                        path={"/"}
                        component={HomePage}
                    />
                    <Route<RouteProps>
                        exact={true}
                        path={"/counter"}
                        component={CounterPage}
                    />
                </Switch>
            </div>
        );
    }
}

export default hot(module)(App);
