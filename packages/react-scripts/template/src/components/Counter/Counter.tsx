import * as React from "react";

interface Props {
    count: number;
    increment(): any;
    decrement(): any;
}

const Counter: React.StatelessComponent<Props> = ({
    decrement,
    increment,
    count,
}) => (
    <div className="Counter">
        <span>{count}</span>
        <button onClick={decrement} data-test-id="decrement">-</button>
        <button onClick={increment} data-test-id="increment">+</button>
    </div>
);

export default Counter;
