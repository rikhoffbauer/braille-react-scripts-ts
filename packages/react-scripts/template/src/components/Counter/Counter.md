Counter example:

```jsx harmony
class Wrapper extends React.Component {
    
    constructor(props, context) {
        super(props, context);
        this.state = { count: 0 };
        this.increment = () => this.setState({count: this.state.count + 1})
        this.decrement = () => this.setState({count: this.state.count - 1})
    }
    
    render() {
        return <Counter count={this.state.count} increment={this.increment} decrement={this.decrement}/>; 
    }
}

<Wrapper/>
```
