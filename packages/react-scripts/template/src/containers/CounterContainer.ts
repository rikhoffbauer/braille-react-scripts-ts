import { connect } from "react-redux";
import { bindActionCreators, Dispatch } from "redux";
import Counter from "../components/Counter/Counter";
import { decrement, getCount, increment, State } from "../redux";

const mapStateToProps = (state: State) => ({
    count: getCount(state),
});

const mapDispatchToProps = (dispatch: Dispatch) =>
    bindActionCreators(
        {
            decrement,
            increment,
        },
        dispatch,
    );

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Counter);
