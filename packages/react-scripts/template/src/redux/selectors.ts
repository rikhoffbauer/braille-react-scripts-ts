/**
 * This file exports all "global" selectors.
 * Usually it just exports all selectors but in some cases it is used to combine
 * redux module selectors,
 *
 * E.g.:
 * A redux module `products` has a selector `getProducts` that gets all products.
 *
 * In our example we want to create a selector that only gets the products
 * that are eligible (i.e. when its minimum age is met).
 *
 * The logged in user has an age property that we can use but is not part
 * of the `products` module but of the `user` module instead.
 *
 * To make this selector we can combine the selector that gets all products and
 * the selector that gets the logged in user.
 *
 * This would look something like:
 *
 * ```
 * export const getEligibleProducts = state => {
 *     const products = productSelectors.getProducts(state);
 *     const loggedInUser = userSelectors.getLoggedInUser(state);
 *
 *     if (!loggedInUser) {
 *         return products;
 *     }
 *
 *     return products.filter(product => product.minAge >= loggedInUser.age);
 * };
 * ```
 *
 * Using `recompose` this would look something like this:
 *
 * ```
 * export const getEligibleProducts = createSelector(
 *    productSelectors.getProducts,
 *    userSelectors.getLoggedInUser,
 *    (products, loggedInUser) => {
 *      if (!loggedInUser) {
 *          return products;
 *      }
 *
 *      return products.filter(product => product.minAge >= loggedInUser.age);
 *    }
 * );
 * ```
 */
import { selectors } from "./modules/root";

export const { getCount } = selectors;
