import { actions } from "./modules/root";

export const { increment, decrement } = actions;
