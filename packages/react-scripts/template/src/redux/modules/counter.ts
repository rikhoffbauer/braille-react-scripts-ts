import { createReducer } from "../../_lib/redux-utils";

export interface State {
    count: number;
}

export const initialState: State = {
    count: 0,
};

// SELECTORS
const getCount = (state: State = initialState) => state.count;

// INCREMENT
const INCREMENT = "counter/INCREMENT";
const increment = () => ({ type: INCREMENT });
const reduceIncrement = (state = initialState) => ({
    ...state,
    count: state.count + 1,
});

// DECREMENT
const DECREMENT = "counter/DECREMENT";
const decrement = () => ({ type: DECREMENT });
const reduceDecrement = (state = initialState) => ({
    ...state,
    count: state.count - 1,
});

// REDUCER
export const reducer = createReducer(initialState, {
    [INCREMENT]: reduceIncrement,
    [DECREMENT]: reduceDecrement,
});

export const types = { INCREMENT, DECREMENT };
export const actions = { increment, decrement };
export const selectors = { getCount };

export default reducer;
