import { createReduxModule } from "../../_lib/redux-utils/module-utils";
import * as counter from "./counter";

export const {
    actions,
    types,
    initialState,
    selectors,
    reducer,
} = createReduxModule({
    counter,
});

export type State = typeof initialState;

export default reducer;
