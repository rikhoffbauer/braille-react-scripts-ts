import { types } from "./modules/root";

export const { DECREMENT, INCREMENT } = types;
