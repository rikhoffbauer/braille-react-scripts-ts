import { applyMiddleware, createStore, DeepPartial } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";
import root from "./modules/root";
import { State } from "./state";

// TODO remove ugly hack
// Due to a change in redux v4 hot reload for the redux devtools has been broken,
// The line below is a workaround for this issue, it can be removed once
// https://github.com/zalmoxisus/redux-devtools-instrument/pull/19
// has been merged and published and the project has updated to this version.
require("redux").__DO_NOT_USE__ActionTypes.REPLACE = "@@redux/INIT";

const configureStore = (preloadedState: DeepPartial<State> = {}) => {
    const composeEnhancers = composeWithDevTools({
        name: process.env.PACKAGE_NAME,
    });

    const store = createStore(
        root,
        preloadedState,
        composeEnhancers(applyMiddleware(thunk)),
    );

    if ((module as any).hot) {
        (module as any).hot.accept("./modules/root", () => {
            store.replaceReducer(require("./modules/root").default);
        });
    }

    return store;
};

export default configureStore;
