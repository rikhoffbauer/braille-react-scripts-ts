import * as React from "react";
import { RouteComponentProps, StaticContext } from "react-router";
import CounterContainer from "../../containers/CounterContainer";
import "./CounterPage.css";

interface Params {}

interface Context extends StaticContext {}

interface Props extends RouteComponentProps<Params, Context> {}

const CounterPage: React.StatelessComponent<Props> = () => (
    <div className="CounterPage">
        <CounterContainer />
    </div>
);

export default CounterPage;
