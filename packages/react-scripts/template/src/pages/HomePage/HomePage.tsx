import * as React from "react";
import { RouteComponentProps, StaticContext } from "react-router";
import "./HomePage.css";

interface Params {}

interface Context extends StaticContext {}

interface Props extends RouteComponentProps<Params, Context> {}

const HomePage: React.StatelessComponent<Props> = () => (
    <div className="HomePage">
        <p className="HomePage__intro">
            To get started, edit <code>src/components/App/App.tsx</code>{" "}
            and save to reload.
        </p>
    </div>
);

export default HomePage;
