import { pipe } from "@ts-commons/fp";
import { BoundSelectorMap, Selector } from "./interfaces";

// binds a pojo of selectors or a single selector to a provided root selector
// used for lifting a module selector into the global redux context.
// returns an object of functions
// that when called first call the rootSelector with the provided state and then
// call the to be selector in the pojo with the return value of the rootSelector
// const getCounterState = state => state.counter;
// e.g.: bindSelectors(getCounterState, counter.selectors)
export const bindSelectors = <
    RootState,
    ModuleState,
    RootSelector extends Selector<RootState, ModuleState>,
    TSelectorMap,
    BoundSelectors extends BoundSelectorMap<TSelectorMap, RootState>
>(
    rootSelector: RootSelector,
    selectorMap: TSelectorMap,
): BoundSelectors =>
    Object.keys(selectorMap).reduce(
        (obj, key) => ({
            ...(obj as any),
            [key]: pipe(
                rootSelector,
                selectorMap[key],
            ),
        }),
        {} as BoundSelectors,
    );
