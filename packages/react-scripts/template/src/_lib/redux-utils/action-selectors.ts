import { Selector } from "./interfaces";
import { Action, ActionMeta, ErrorAction } from "./interfaces";

export type ActionSelector<Value> = Selector<Action<any>, Value>;
export type ActionPredicate = ActionSelector<boolean>;

// BOOLEAN SELECTORS (I.E. PREDICATES)

// returns true if the action is of the provided type (action.type === type)
export const isOfType: (type: string) => ActionPredicate = type => action =>
    action.type === type;

// returns true if the action has a payload
export const hasPayload: ActionPredicate = action => action.payload != null;

// returns true if the action is successful, i.e. not loading and not erroneous
export const isSuccessful: ActionPredicate = action =>
    !action.error && (!action.meta || !action.meta.loading);

// returns true if the action is loading (action.meta.loading is true)
export const isLoading: ActionPredicate = action =>
    Boolean(action.meta && action.meta.loading);

export const isErroneous = <T extends Action>(
    action: T,
): T extends ErrorAction ? true : false => Boolean(action.error) as any;

// "NORMAL" SELECTORS (I.E. ALL VALUE TYPE SELECTORS)

// gets the payload of the action
export const getPayload = <T>(action: Action<T>): T => action.payload as T;

// gets the error if there is one
export const getError = <T extends Error>(action: Action<any>): T =>
    action.error ? action.payload : undefined;

// returns true if the payload is equal to the provided payload
export const payloadEquals = <T>(payload: T) => (action: Action<T>): boolean =>
    action.payload === payload;

// gets the metadata of the action
export const getMeta = <T extends ActionMeta>(action: Action<any, T>): T =>
    action.meta as T;
