import { Action, combineReducers } from "redux";
import { Reducer, Selector } from "./interfaces";
import { bindSelectors } from "./selector-utils";

export type ReducersMapObject<S = any, A extends Action = Action> = {
    [K in keyof S]: Reducer<S[K], A>;
}

export interface ReduxModuleMap<State, Actions, Selectors, Types> {
    [name: string]: ReduxModule<State, Actions, Selectors, Types>;
}

export interface ReduxModule<State, Actions, Selectors, Types> {
    actions: Actions;
    initialState: State;
    reducer: Reducer<State>;
    selectors: Selectors;
    types: Types;
}

export interface IntermediaryReduxModule<State, Actions, Selectors, Types> {
    actions: Actions;
    initialState: State;
    reducer: ReducersMapObject<State>;
    selectors: Selectors;
    types: Types;
}

export const createReduxModule = <State, Actions, Selectors, Types>(
    moduleMap: ReduxModuleMap<State, Actions, Selectors, Types>,
): ReduxModule<State, Actions, Selectors, Types> => {
    const getModuleState = (key: string): Selector<any, any> => (
        { [key]: value } = result.initialState,
    ) => value;
    const result: IntermediaryReduxModule<
        State,
        Actions,
        Selectors,
        Types
    > = Object.keys(moduleMap).reduce(
        (obj, key) =>
            ({
                actions: {
                    ...(obj.actions as {}),
                    ...(moduleMap[key].actions as {}),
                },
                initialState: {
                    ...(obj.initialState as {}),
                    [key]: moduleMap[key].initialState,
                },
                reducer: {
                    ...(obj.reducer as {}),
                    [key]: moduleMap[key].reducer,
                },
                selectors: {
                    ...(obj.selectors as {}),
                    ...(bindSelectors(
                        getModuleState(key),
                        moduleMap[key].selectors,
                    ) as {}),
                },
                types: {
                    ...(obj.types as {}),
                    ...(moduleMap[key].types as {}),
                },
            } as IntermediaryReduxModule<State, Actions, Selectors, Types>),
        {
            actions: {},
            initialState: {},
            reducer: {},
            selectors: {},
            types: {},
        } as IntermediaryReduxModule<State, Actions, Selectors, Types>,
    );

    return { ...result, reducer: combineReducers<State>(result.reducer) };
};
