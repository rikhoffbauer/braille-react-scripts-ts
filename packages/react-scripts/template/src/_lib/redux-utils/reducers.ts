// creates a reducer that provides initial state
import { Reducer } from "./interfaces";

export const initialStateReducer = <State>(
    initialState: State,
): Reducer<State> => (state = initialState) => state;
