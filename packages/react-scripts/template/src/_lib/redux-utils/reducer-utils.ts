import { and, not } from "@ts-commons/fp/operators";
import { ActionPredicate } from "./action-selectors";
import { Action, Reducer, Transformer } from "./interfaces";

// used for combining multiple reducers into one.
// reducers are ran in the order they are provided, with each reducer getting
// the resulting state of the previous reducer
// or the initially provided state in case of the first reducer
export const mergeReducers = <State>(
    ...reducers: Array<Reducer<State>>
): Reducer<State> => (state, action) =>
    reducers.reduce((newState, reducer) => reducer(newState, action), state);

// reduce an action if all of the provided predicates match
// returns a function that takes a createTransformer function or a reducer
// the createTransformer function has the same call signature as a reducer
// but returns a state transformer (state => state)
export const reduceIfAction = (
    ...predicates: ActionPredicate[]
): (<State>(
    createTransformer:
        | ((state: State, action: Action<any>) => Transformer<State>)
        | Reducer<State>,
) => Reducer<State>) => createTransformer => (state, action) => {
    if (not(and(...predicates))(action)) {
        return state;
    }

    const transformer = createTransformer(state, action);

    if (typeof transformer !== "function") {
        return typeof transformer === "undefined" ? state : transformer;
    }

    return transformer(state);
};

export const createReducer = <State>(
    initialState: State,
    reducerMap: { [name: string]: Reducer<State> },
): Reducer<State> => (state = initialState, action) => {
    if (!reducerMap[action.type]) {
        return state;
    }

    return reducerMap[action.type](state, action);
};
