export interface ActionMeta {
    loading?: boolean;
}

export type Transformer<State> = (state?: State) => State;

export interface Action<Payload = any, Meta extends ActionMeta = ActionMeta> {
    type: string;
    error?: boolean;
    payload?: Payload;
    meta?: ActionMeta;
}

export interface ErrorAction<
    T extends Error = Error,
    Meta extends ActionMeta = ActionMeta
> extends Action<T, Meta> {}

export type Reducer<
    State,
    Payload = any,
    Meta extends ActionMeta = ActionMeta
> = (state: State, action: Action<Payload, Meta>) => State;

export type Selector<State, Value> = (state: State) => Value;

// TODO move to @ts-commons/lang
export type ReturnType<T> = T extends (...args: any[]) => infer R ? R : never;

export type BoundSelectorMap<SelectorMap, RootState> = {
    [P in keyof SelectorMap]: (state: RootState) => ReturnType<SelectorMap[P]>
};
