This project was bootstrapped with @braille/react-scripts-ts, a fork of react-scripts-ts.

See the FB_README.md file for the original README.

## Getting started

```bash
npx create-react-app --scripts-version=@braille/react-scripts-ts PROJECT_NAME
cd PROJECT_NAME
yarn start
```

## Features

-   supports gitlab auto devops with deployments to kubernetes
-   typescript
-   tslint
-   postcss
-   docker
-   redux
-   react-router
-   react-hot-loader
-   react-styleguidist
-   jest-stare
-   wallaby
-   husky
-   prettier

## React-router

The devserver and the server used for deployments both have a fallback for the history api
so the `BrowserRouter` is used by default.  
This can be changed in in `src/index.tsx`.

## Redux

#### Modules

Redux has been setup using the ducks/modules structure.
Each redux module exports the following values:

| Export name  | Type        | Description                                                                        |
| ------------ | ----------- | ---------------------------------------------------------------------------------- |
| actions      | `{}`        | A pojo of action creators.                                                         |
| initialState | `State`     | The initial state of the module,                                                   |
| reducer      | `Function`  | The reducer of the redux module, reduces the modules actions to the modules state. |
| selectors    | `{}`        | A pojo of local selectors<sup>1</sup>,                                             |
| State        | `interface` | The shape of the state of the module in the form of a typescript `interface`,      |
| types        | `{}`        | A pojo of action types,                                                            |
| **default**  | `Function`  | The `reducer` is the default export.                                               |

_<sup>1</sup> local selectors are selectors that select a value from the **module's** state, not the global (store) state_.

In a nested module structure the parent node should export its children's `types`, `actions` and `selectors`
on its own `types`, `actions` and `selectors` exports.
The `selectors` exported from child modules should be lifted into the context (state shape) of the parent module.
This is best explained through an example:

##### child module

```typescript
//...
const initialState = {
    count: 0,
};
//...
const getCount = (state = initialState) => state.count;
//...
export const selectors = { getCount };
```

##### parent module

```typescript
import * as counter from "./counter";
//...
export const initialState = {
    counter: counter.initialState,
};
//...
const getCount = (state = initialState) =>
    counter.selectors.getCount(state.counter);
//...
export const selectors = { getCount };
```

To facilitate this the `createReduxModule` helper can be used:

##### parent module

```typescript
import { createReduxModule } from "@braille/redux-utils";
import * as counter from "./counter";

export const {
    actions,
    types,
    initialState,
    selectors,
    reducer,
} = createReduxModule({
    counter,
});

export type State = typeof initialState;

export default reducer;
```

## Postcss

By default an embedded postcss config is used which can be found at `@braille/react-scripts-ts/config/postcss.config.js`.
To override the default configuration add a postcss.config.js in the project root.

## Husky

The project uses `git hooks` created by `husky`.

_`git hooks` are scripts that are ran before or after a git action, such as committing or pushing._

This project has the following hooks:

-   `commit`
    -   lint staged files
-   `push`
    -   run tests
