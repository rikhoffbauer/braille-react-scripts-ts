module.exports = {
    webpackConfig: require('@braille/react-scripts-ts/config/webpack.config.dev'),
    propsParser: require('react-docgen-typescript').withCustomConfig(
        './tsconfig.json'
    ).parse,
};
